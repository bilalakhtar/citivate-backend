
var mongoose = require("mongoose");

var Community = mongoose("Community", {
	name: String,
	lat: Number,
	lon: Number
});

var User = mongoose.model("User", {
	name: String,
	email: String,
	password: String,
	ageGroup: Number,
	gender: Number, // 0 for male, 1 for female, 2 for not disclosed.
	community: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Community"
	},

	rank: {type: Number, default: -1},
	points: {type: Number, default: 0},

	achievements: mongoose.Schema.Types.ObjectId
});

var Place = mongoose.model("Place", {
	title: "String",
	type: "String"
});

var Achievement = mongoose.model("Achievement", {
	title: String,
	place: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User"
	},


});
