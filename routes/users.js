var express = require('express');
var passport = require('passport');
var router = express.Router();

var mongoose = require('mongoose');
var User = mongoo

/* GET users listing. */
router.get('/', function(req, res) {
  res.send('respond with a resource');
});

router.post('/signup', function(req, res){
	var name  		= req.body.name;
	var email 		= req.body.email;
	var password 	= req.body.password;
	var ageGroup 	= parseInt(req.body.ageGroup);
	var gender 		= parseInt(req.body.gender);

	var newUser = new User({
		name: name,
		email: email,
		ageGroup: ageGroup,
		gender: gender
	});

	bcrypt.hash(password, 8, function(err, hash){
		if (err) return res.status(500).send({error: err});
		newUser.password = hash;
		newUser.save(function(err){
			if (err) return res.status(500).send({error: err});

			res.send({success: 1});
		});
	});
});

router.get('/testauth', passport.authenticate("basic", {session: false}), function(req, res){
	res.send(req.user);
});

module.exports = router;
