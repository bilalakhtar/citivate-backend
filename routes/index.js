var express = require('express');
var router = express.Router();
var passport = require('passport');

router.use(passport.authenticate("basic", {session: false}));

/* GET home page. */
router.get('/', function(req, res) {
	var response = {
		user: {
			name: req.user.name,
			email: req.user.email,
			rank: req.user.rank,
			points: req.user.points
		}
	}

	res.send(response);
});

module.exports = router;
